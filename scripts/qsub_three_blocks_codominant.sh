#!/bin/sh
#$ -N sim_3bc
#$ -cwd
#$ -l h_rt=24:00:00 
#$ -l h_vmem=32G

set -eu

OUTPATH=${1:-}
SUFFIX=${2:-}
LOCI=${3:-}
DOBREEDING=${4:-}
NEDITS=${5:-}
DISCOVERY=${6:-}
DOAVOIDANCE=${7:-}
NAVOID=${8:-}

. /etc/profile.d/modules.sh

module load R/3.3.2

Rscript R/sim_three_blocks_codominant.R $OUTPATH $SUFFIX $LOCI $DOBREEDING $NEDITS $DISCOVERY $DOAVOIDANCE $NAVOID
