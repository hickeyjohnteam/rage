


if [ ! -d simulations/3br ]; then
    mkdir simulations/3br
    mkdir simulations/3br/load
    mkdir simulations/3br/direct_comparison
    mkdir simulations/3br/breeding
    mkdir simulations/3br/breeding_perfect
    mkdir simulations/3br/avoidance
    mkdir simulations/3br/discovery
    mkdir simulations/3br/avoidance_discovery
fi

## Load simulations
for LOCI in 5000 10000 15000;
do

    for I in {1..10};
    do
	
	qsub scripts/qsub_three_blocks_recessive.sh \
	     simulations/3br/load/ $I $LOCI FALSE 0 0 FALSE 0
	
    done
    
done

## Direct comparision simulations
for I in {1..50};
do
    
    qsub scripts/qsub_three_blocks_direct_comparison_recessive.sh \
	 simulations/3br/direct_comparison/ $I 10000 0.75
    
done

## Editing simulations
for NEDITS in 1 5 20;
do

    for I in {1..50};
    do
	
	qsub scripts/qsub_three_blocks_recessive.sh \
	     simulations/3br/breeding/${NEDITS}edits_ $I 10000 TRUE $NEDITS 0.75 FALSE 0
	
    done
    
done


## Editing simulations with perfect discovery
for NEDITS in 1 5 20;
do

    for I in {1..50};
    do
	
	qsub scripts/qsub_three_blocks_recessive.sh \
	     simulations/3br/breeding_perfect/${NEDITS}edits_ $I 10000 TRUE $NEDITS 1 FALSE 0
	
    done
    
done



## Avoidance simulations
for NAVOID in 100 250 500;
do

    for I in {1..50};
    do
	
	qsub scripts/qsub_three_blocks_recessive.sh \
	     simulations/3br/avoidance/${NAVOID}avoid_ $I 10000 FALSE 0 0.75 TRUE $NAVOID
	
    done
    
done

## Discovery simulations
for STRATEGY in random high_frequency low_frequency frequency_threshold;
do

    for I in {1..50};
    do
	
	qsub scripts/qsub_three_blocks_discovery_recessive.sh \
	     simulations/3br/discovery/${STRATEGY}_discovery_ $I 10000 TRUE 5 $STRATEGY FALSE 0
	
    done
    
done



## Avoidance discovery simulations
# for STRATEGY in total_load heterozygous_load homozygous_load load_frequency;
# do

#     for I in {1..10};
#     do
	
# 	qsub scripts/qsub_three_blocks_discovery_recessive.sh \
# 	     simulations/3br/avoidance_discovery/${STRATEGY}_discovery_ $I 10000 FALSE 0 $STRATEGY TRUE 50
	
#     done
    
# done
