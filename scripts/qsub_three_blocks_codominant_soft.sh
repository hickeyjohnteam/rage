#!/bin/sh
#$ -N sim_3bcs
#$ -cwd
#$ -l h_rt=24:00:00 
#$ -l h_vmem=24G

set -eu

OUTPATH=${1:-}
SUFFIX=${2:-}

. /etc/profile.d/modules.sh

module load R/3.3.2

Rscript R/sim_three_blocks_codominant_soft.R $OUTPATH $SUFFIX
