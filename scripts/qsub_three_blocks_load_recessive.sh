#!/bin/sh
#$ -N sim_3br_load
#$ -cwd
#$ -l h_rt=24:00:00 
#$ -l h_vmem=32G

set -eu

OUTPATH=${1:-}
SUFFIX=${2:-}
LOCI=${3:-}
GAMMA=${4:-}
N_BURNIN=${5:-}
N_HISTORICAL=${6:-}
HISTORY=${7:-}

. /etc/profile.d/modules.sh

module load R/3.3.2

Rscript R/sim_three_blocks_load_recessive.R $OUTPATH $SUFFIX $LOCI $GAMMA $N_BURNIN $N_HISTORICAL $HISTORY
