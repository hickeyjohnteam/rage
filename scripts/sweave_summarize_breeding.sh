#!/bin/sh
#$ -N sweave_summarize_breeding
#$ -cwd
#$ -l h_rt=8:00:00 
#$ -l h_vmem=200G

set -eu

. /etc/profile.d/modules.sh

module load R/3.3.2

if [ ! -d figures ]; then
    mkdir figures
fi

if [ ! -d tables ]; then
    mkdir tables
fi

R CMD Sweave sweave/summarize_3bc_breeding.Rnw
R CMD Sweave sweave/summarize_3bc_avoidance.Rnw
R CMD Sweave sweave/summarize_3bc_discovery.Rnw
R CMD Sweave sweave/summarize_3bc_breeding_perfect.Rnw
R CMD Sweave sweave/summarize_3bc_mortality.Rnw

R CMD Sweave sweave/summarize_3br_breeding.Rnw
R CMD Sweave sweave/summarize_3br_avoidance.Rnw
R CMD Sweave sweave/summarize_3br_discovery.Rnw
R CMD Sweave sweave/summarize_3br_breeding_perfect.Rnw
R CMD Sweave sweave/summarize_3br_mortality.Rnw

Rscript R/gain_combined_plots.R
Rscript R/difference_combined_plots.R
Rscript R/condition_combined_plot.R 
