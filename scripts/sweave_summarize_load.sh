#!/bin/sh
#$ -N sweave_summarize_load
#$ -cwd
#$ -l h_rt=2:00:00 
#$ -l h_vmem=100G

set -eu

. /etc/profile.d/modules.sh

module load R/3.3.2

if [ ! -d figures ]; then
    mkdir figures
fi

if [ ! -d tables ]; then
    mkdir tables
fi

R CMD Sweave sweave/summarize_3bc_load.Rnw
R CMD Sweave sweave/summarize_3br_load.Rnw
R CMD Sweave sweave/summarize_3bc_daf.Rnw
R CMD Sweave sweave/summarize_3br_daf.Rnw
Rscript R/load_combined_plots.R
