#!/bin/sh
#$ -N sim_3br_mortality
#$ -cwd
#$ -l h_rt=24:00:00 
#$ -l h_vmem=24G

set -eu

OUTPATH=${1:-}
SUFFIX=${2:-}
LOCI=${3:-}
DOBREEDING=${4:-}
MORTALITY=${5:-}
DISCOVERY=${6:-}
DOAVOIDANCE=${7:-}
NAVOID=${8:-}

. /etc/profile.d/modules.sh

module load R/3.3.2

Rscript R/sim_three_blocks_mortality_recessive.R $OUTPATH $SUFFIX $LOCI $DOBREEDING $MORTALITY $DISCOVERY $DOAVOIDANCE $NAVOID
