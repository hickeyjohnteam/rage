// [[Rcpp::depends(RcppArmadillo)]]

#include <cmath> 
#include <RcppArmadillo.h>
#include <iostream>


/*
  C++ functions for adding a condition trait to AlphaSimR.

*/


// ALPHASIMR FUNCTIONS

/*
 * Verbatim from AlphaSimR. Returns an 1 if heterozygous.
 */

// Converts geno matrix to dominance indicator matrix when ploidy=2
// Using imat instead of Mat<unsigned char> for increased speed
// [[Rcpp::export]]
arma::imat getDomGeno(const arma::Mat<unsigned char>& geno){
  arma::imat output(geno.n_rows,geno.n_cols);
  output = arma::conv_to<arma::imat>::from(geno);
  output -= 1;
  output = 1-arma::abs(output);
  return output;
}


/*
 * Verbatim from AlphaSimR.
 * Genotype data is stored in a field of cubes.
 * The field has length equal to nChr
 * Each cube has dimensions nLoci by ploidy by nInd
 */
// [[Rcpp::export]]
arma::Mat<unsigned char> getGeno(const arma::field<arma::Cube<unsigned char> >& geno, 
                                 const arma::ivec& lociPerChr,
                                 arma::uvec lociLoc){
  // R to C++ index correction
  lociLoc -= 1;
  
  int nInd = geno(0).n_slices;
  int nChr = geno.n_elem;
  arma::Mat<unsigned char> output(nInd,arma::sum(lociPerChr), arma::fill::zeros);
  int loc1;
  int loc2 = -1;
  for(int i=0; i<nChr; ++i){
    // Get loci locations
    loc1 = loc2+1;
    loc2 += lociPerChr[i];
    arma::uvec chrLociLoc = lociLoc(arma::span(loc1,loc2));
    // Get chromsome genotype
    arma::Mat<unsigned char> tmp;
    tmp = arma::sum(geno(i),1);
    // Assign genotypes to output matrix
    output.cols(loc1,loc2) = (tmp.rows(chrLociLoc)).t();
  }
  return output;
}





// NEW FUNCTIONS

// Calculates genetic values for multiplicative condition effects
arma::vec calcGvCondition(const arma::Mat<unsigned char>& geno,
			  const arma::vec& a,
			  const arma::vec& d,
			  double intercept){

  arma::vec output(geno.n_rows);
  for (int i = 0; i < output.n_elem; i++) { // each individual
    double survival = 1;
    for (int j = 0; j < geno.n_cols; j++) { // each locus

      switch (geno(i, j)) {
        case 1 : survival = survival * (1 - a(j) * d(j)); break;
        case 2 : survival = survival * (1 - a(j));
      }
      
    }
    output(i) = survival;
  }
  return output + intercept;
}



// Retrieves genetic values for condition
// [[Rcpp::export]]
arma::vec getGvCondition(const Rcpp::S4& trait, const Rcpp::S4& pop){
  arma::vec a = trait.slot("addEff");
  arma::vec d = trait.slot("domEff");
  double intercept = trait.slot("intercept");
  arma::Mat<unsigned char> geno;
  geno = getGeno(pop.slot("geno"), 
                 trait.slot("lociPerChr"),
                 trait.slot("lociLoc"));
  return calcGvCondition(geno, a, d, intercept);
}


// Remove an individual from genotype data
arma::field<arma::Cube<unsigned char> > removeIndividualGeno(arma::field<arma::Cube<unsigned char> >& geno,
							     int indLoc) {
  indLoc -= 1;
  for (int i = 0; i < geno.n_elem; i++) { // each chromosome
      geno(i).shed_slice(indLoc);
  }
  
  return geno;
}

// Remove a vector of individuals from genotype data
// [[Rcpp::export]]
arma::field<arma::Cube<unsigned char> > removeIndividualsGeno(arma::field<arma::Cube<unsigned char> >& geno,
							      arma::uvec indLoc) {
  indLoc -= 1;
  indLoc = sort(indLoc);
  for (int i = 0; i < geno.n_elem; i++) { // each chromosome
    for (int j = indLoc.n_elem - 1; j >= 0; j--) { //each individual
      geno(i).shed_slice(indLoc(j));
    }
  }
  
  return geno;
}

// Add deleterious mutations to genotype data; allows no back mutation
// [[Rcpp::export]]
arma::field<arma::Cube<unsigned char> > mutateConditionGeno(arma::field<arma::Cube<unsigned char> >& geno,
							    const arma::ivec& lociPerChr,
							    arma::uvec lociLoc,
							    double pMutation) {
  lociLoc -= 1;

  int nInd = geno(0).n_slices;
  int nChr = geno.n_elem;
  int chrOffset = 0;
  
  for (int chr = 0; chr < nChr; chr++) { // each chromosome
    for (int ind = 0; ind < nInd; ind++) { // each individual
      for (int hap = 0; hap < 2; hap++) { // each haplotype
	arma::vec mutationDraw = arma::vec(lociPerChr(chr));
	mutationDraw.randu();
	for (int loc = chrOffset; loc < chrOffset + lociPerChr(chr); loc++) { // each locus
	  if (mutationDraw(loc - chrOffset) < pMutation) {
	    geno(chr).slice(ind)(lociLoc(loc), hap) = 1;
	  }
	}
      }
    }
    chrOffset += lociPerChr(chr);
  }
  return geno;
}


// Edit deleterious mutations
//arma::field<arma::Cube<unsigned char> >
// [[Rcpp::export]]
Rcpp::List editConditionGeno(arma::field<arma::Cube<unsigned char> >& geno,
							  const int nEdits,
							  arma::uvec editChr,
							  arma::uvec editLoc) {
  editLoc -= 1;
  editChr -= 1;
  
  int nInd = geno(0).n_slices;
  arma::uvec editsPerInd (nInd);
  editsPerInd.fill(0);

  arma::uvec locusEdited (editLoc.n_elem);
  locusEdited.fill(0);

  for (int edit = 0; edit < editLoc.n_elem; edit++) { // each position to edit
    for (int ind = 0; ind < nInd; ind++) { // each individual
      bool individualEdited = false;

      for (int hap = 0; hap < 2; hap++) { // each haplotype
	if (editsPerInd(ind) < nEdits &&
	    geno(editChr(edit)).slice(ind)(editLoc(edit), hap) != 0) {
	  geno(editChr(edit)).slice(ind)(editLoc(edit), hap) = 0;
	  individualEdited = true;
	}
      }
      if (individualEdited) {
	editsPerInd(ind)++;
	locusEdited(edit)++;
      }
    }
  }
    std::cout <<  sum(editsPerInd) << std::endl;
    //    std::cout <<  max(editsPerInd) << std::endl;
    //std::cout <<  editsPerInd.n_elem << std::endl;
    //  return geno;

  return Rcpp::List::create(Rcpp::Named("geno") = geno,
			    Rcpp::Named("numberEdits") = sum(editsPerInd),
			    Rcpp::Named("locusEdited") = locusEdited);
}




// [[Rcpp::export]]
arma::vec calcChrWildtypeFreq(const arma::Cube<unsigned char>& geno,
			      int ploidy){
  arma::Mat<unsigned char> tmp = arma::sum(geno,1);
  arma::vec output = arma::mean(arma::conv_to<arma::mat>::from(tmp),
                                1)/ploidy;
  return output;
}
