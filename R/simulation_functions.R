

## Helper functions for simulating selection with condition in AlphaSimR

library(assertthat)


### SIMULATION FUNCTION

make_base_population <- function(nInd = 10000,
                                 nChr = 2,
                                 segSites = 5000,
                                 nQtlPerChr = 1000,
                                 nConditionPerChr = 1000,
                                 maxQtl = 1000,
                                 maxSnp = 0,
                                 h,
                                 distribution,
                                 pCutoff = 0.001,
                                 nSnpPerChr = 0,
                                 populationHistory = "PIG",
                                 gammaTrait = NULL) {
    
    
    FOUNDERPOP <- runMacs(nInd = nInd,
                          nChr = nChr,
                          segSites = segSites,
                          inbred = FALSE,
                          species = populationHistory)

    print("Simulation parameters")

    SIMPARAM <- createSimulation(maxQtl = maxQtl,
                                 maxSnp = maxSnp,
                                 snpQtlOverlap = FALSE,
                                 gender = "yes_sys",
                                 founderPop = FOUNDERPOP,
                                 minSnpFreq = 0) %>%
        addTraitA(nQtlPerChr = nQtlPerChr,
                  meanG = 100,
                  varG = 10,
                  founderPop = FOUNDERPOP,
                  simParam = .)

    if (!is.null(gammaTrait)) {
        SIMPARAM <- createSimulation(maxQtl = maxQtl,
                                     maxSnp = maxSnp,
                                     snpQtlOverlap = FALSE,
                                     gender = "yes_sys",
                                     founderPop = FOUNDERPOP,
                                     minSnpFreq = 0) %>%
            addTraitA(nQtlPerChr = nQtlPerChr,
                      meanG = 100,
                      varG = 10,
                      gamma = TRUE,
                      shape = gammaTrait,
                      founderPop = FOUNDERPOP,
                      simParam = .)
        
    }

    SIMPARAM <- addTraitCondition(nQtlPerChr = nConditionPerChr,
                                  h = h,
                                  distribution = distribution,
                                  pCutoff = pCutoff,
                                  founderPop = FOUNDERPOP,
                                  simParam = SIMPARAM)

    varE <- c(20, 0)

    if (nSnpPerChr > 0) {
        SIMPARAM <- addSnpChip(nSnpPerChr, simParam = SIMPARAM)
    }
    
    print("Population creation")
    
    population <- newPop(FOUNDERPOP,
                         simParam = SIMPARAM) %>%
        setPheno(varE = varE,
                 simParam = SIMPARAM) %>%
        setCondition(conditionTraitNumber = 2,
                     simParam = SIMPARAM)

    population <- removeDeadIndividuals(population,
                                        conditionTraitNumber = 2)
    
    list(FOUNDERPOP = FOUNDERPOP,
         SIMPARAM = SIMPARAM,
         population = population)
}


purifying_selection_discard <- function(base_population,
                                        n_gen_random = 50,
                                        SIMPARAM,
                                        p_mutation = 0,
                                        n_crosses = 200,
                                        n_sires = 25,
                                        n_dams = 200,
                                        progeny = 10) {
    print("Purifying selection")
    
    generation <- base_population
    N <- numeric(n_gen_random)
    performance <- numeric(n_gen_random)
    condition <- numeric(n_gen_random)

    N[1] <- generation@nInd
    performance[1] <- meanG(generation)[1]
    condition[1] <- meanG(generation)[2]


    varE <- c(20, 0)
    
    for (i in 2:(n_gen_random)) {
        if (i %% 10 == 0) {
            print(i)
        }
    
        generation <- randCross(pop = generation,
                                nCrosses = n_crosses,
                                nProgeny = progeny,
                                simParam = SIMPARAM) %>%
            setPheno(varE = varE,
                     simParam = SIMPARAM) %>%
            setCondition(conditionTraitNumber = 2,
                         simParam = SIMPARAM)

        generation <- removeDeadIndividuals(generation, conditionTraitNumber = 2)
        
        if (p_mutation > 0) {
            generation <- mutateCondition(generation,
                                          2,
                                          p_mutation,
                                          simParam = SIMPARAM)
        }
        N[i] <- generation@nInd
        performance[i] <- meanG(generation)[1]
        condition[i] <- meanG(generation)[2]
    }

    list(population = generation,
         results = data.frame(gen = 1:n_gen_random, N, performance, condition))
}



breeding_discard <- function(population,
                             n_gen_breeding = 40,
                             nedits = 0,
                             edit_strategy = "effect",
                             discovery = 1,
                             SIMPARAM,
                             p_mutation = 0,
                             cross_multiplier = 1,
                             n_sires = 25,
                             n_dams = 200,
                             progeny,
                             navoid = 0,
                             avoidance_strategy = "total_load",
                             mortality = 0) {

    print("Breeding")

    generation <- population
    
    N <- numeric(n_gen_breeding)
    Ntheo <- numeric(n_gen_breeding)
    performance <- numeric(n_gen_breeding)
    varG <- numeric(n_gen_breeding)
    condition <- numeric(n_gen_breeding)
    mean_heterozygous_load <- numeric(n_gen_breeding)
    mean_total_load <- numeric(n_gen_breeding)
    sd_heterozygous_load <- numeric(n_gen_breeding)
    sd_total_load <- numeric(n_gen_breeding)
    number_edits <- numeric(n_gen_breeding)


    deleterious_allele_frequencies <- vector(mode = "list", length = n_gen_breeding)

    distinct_loci_edited <- vector(length = n_gen_breeding, mode = "list")

    ## Discover loci for editing
    if (discovery < 1) {
        geno <- pullQtlGeno(population, 2, simParam = SIMPARAM)
        segregating <- which(colSums(geno) > 0 & colSums(geno) < nrow(geno) * 2)
        discovered <- sample(segregating, round(length(segregating) * discovery))

        snp_geno <- pullSnpGeno(population, 1, simParam = SIMPARAM)
        segregating_snp <- which(colSums(snp_geno) > 0 & colSums(snp_geno) < nrow(snp_geno) * 2)
        false_discovered <- sample(segregating_snp,
                                   length(segregating) - round(length(segregating) * discovery))
    } else {
        ## Passing null allows all loci to be edited
        discovered <- NULL
        false_discovered <- NULL
    }

    ## Save data on first generation
    N[1] <- generation@nInd
    Ntheo[1] <- generation@nInd
    performance[1] <- meanG(generation)[1]
    condition[1] <- meanG(generation)[2]
    varG[1] <- varG(generation)[1]
            
    heterozygous_load <- heterozygous_load(generation, 2, SIMPARAM)
    total_load <- total_load(generation, 2, SIMPARAM)
            
    mean_heterozygous_load[1] <- mean(heterozygous_load)
    sd_heterozygous_load[1] <- sd(heterozygous_load)
    mean_total_load[1] <- mean(total_load)
    sd_total_load[1] <- sd(total_load)

    geno <- pullQtlGeno(generation, 2, simParam = SIMPARAM)
    deleterious_allele_frequencies[[1]] <- colSums(geno)/(2 * nrow(geno))
    
    for (i in 2:n_gen_breeding) {
        if (i %% 10 == 0) {
            print(i)
        }
        
        nSires <- min(n_sires, sum(generation@gender == "M"))
        nDams <- min(n_dams, sum(generation@gender == "F"))
        if (nSires < 25 || nDams < 25) {
            ## Population is dead
            break
        } else {
            ## Proceed with selection
            if (navoid == 0) {
                sires <- selectMale(pop = generation,
                                    nInd = nSires,
                                    trait = 1,
                                    use = "gv",
                                    simParam = SIMPARAM)
            } else {
                kept <- avoidIndividuals(generation[which(generation@gender == "M")],
                                         2,
                                         nRemove = navoid,
                                         strategy = avoidance_strategy,
                                         discoveredLoci = discovered,
                                         falseDiscoveredLoci = false_discovered,
                                         simParam = SIMPARAM)

                ## Check that we have enough sires left
                if (sum(kept@gender == "M") < 25) {
                    break
                }
                
                sires <- selectMale(pop = kept,
                                    nInd = nSires,
                                    trait = 1,
                                    use = "gv",
                                    simParam = SIMPARAM)
            }
            
            if (nedits > 0) {
                surviving_sires <- sires
                n_sires_before_mortality <- sires@nInd
                if (mortality > 0) {
                    ## Apply mortality
                    surviving_sires <- surviving_sires[runif(n_sires_before_mortality, 0, 1) >
                                                       mortality]
                }
                n_sires_after <- surviving_sires@nInd
                edited_sires <- editCondition(surviving_sires,
                                              2,
                                              nedits,
                                              simParam = SIMPARAM,
                                              strategy = edit_strategy,
                                              discoveredLoci = discovered,
                                              falseDiscoveredLoci = false_discovered)
                sires <- edited_sires$population
                number_edits[i] <- edited_sires$numberEdits
                distinct_loci_edited[[i]] <- edited_sires$distinctLociEdited

                if (mortality > 0) {
                    ## If needed, get extra sires to replace the dead ones
                    extras_needed <- n_sires_before_mortality - n_sires_after
                    if (extras_needed > 0) {    
                        remaining_individuals <- generation[!(generation@id %in% sires@id)]
                        extra_sires <- selectMale(pop = remaining_individuals,
                                                  nInd = min(remaining_individuals@nInd,
                                                             extras_needed),
                                                  trait = 1,
                                                  use = "gv",
                                                  simParam = SIMPARAM)
                        sires <- c(sires,
                                   extra_sires)
                    }
                }
            }
            
            dams <- selectFemale(pop = generation,
                                 nInd = nDams,
                                 trait = 1,
                                 use = "gv",
                                 simParam = SIMPARAM)


            ## Normal selection
            offspring <- randCross2(males = sires,
                                    females = dams,
                                    nCrosses = nDams * cross_multiplier,
                                    nProgeny = progeny,
                                    simParam = SIMPARAM) %>%
                setPheno(varE = c(20, 0),
                         simParam = SIMPARAM) %>%
                setCondition(conditionTraitNumber = 2,
                             simParam = SIMPARAM)

            offspring <- removeDeadIndividuals(offspring, conditionTraitNumber = 2)
            generation <- offspring
            
            if (p_mutation > 0) {
                generation <- mutateCondition(generation,
                                              2,
                                              p_mutation,
                                              simParam = SIMPARAM)
            }
            N[i] <- generation@nInd
            Ntheo[i] <- offspring@nInd
            performance[i] <- meanG(generation)[1]
            condition[i] <- meanG(generation)[2]
            varG[i] <- varG(generation)[1]
            
            heterozygous_load <- heterozygous_load(generation, 2, SIMPARAM)
            total_load <- total_load(generation, 2, SIMPARAM)
            
            mean_heterozygous_load[i] <- mean(heterozygous_load)
            sd_heterozygous_load[i] <- sd(heterozygous_load)
            mean_total_load[i] <- mean(total_load)
            sd_total_load[i] <- sd(total_load)

            geno <- pullQtlGeno(generation, 2, simParam = SIMPARAM)
            deleterious_allele_frequencies[[i]] <- colSums(geno)/(2 * nrow(geno))
        }
    }
    list(last_generation = generation,
         results = data.frame(gen = 1:n_gen_breeding, N, Ntheo, performance, condition, varG, number_edits,
                              mean_heterozygous_load, mean_total_load,
                              sd_heterozygous_load, sd_total_load),
         deleterious_allele_frequencies = deleterious_allele_frequencies,
         discovered_condition_loci = discovered,
         distinct_loci_edited = distinct_loci_edited)
}







## SUMMARY FUNCTIONS

summarise_results <- function(generations) {

    performance <- sapply(generations, function(x) meanG(x)[1])
    condition <- sapply(generations, function(x) meanG(x)[2])
    variance <- sapply(generations, function(x) varG(x)[1])
    N <- sapply(generations, function(x) x@nInd)
    
    
    results <- data.frame(gen = 1:(n_gen_random + n_gen_breeding),
                          performance,
                          condition,
                          N)
}



segregating_qtl <- function(population, trait_number, SIMPARAM) {
    if (population@nInd > 0) {
        geno <- pullQtlGeno(population, trait_number, simParam = SIMPARAM)
        fs <- colSums(geno)/nrow(geno)
        return(fs[fs > 0 & fs < 1])
    } else {
        return(NA)
    }
}

total_load <- function(population, trait_number, SIMPARAM) {
    if (population@nInd > 0) {
        geno <- pullQtlGeno(population, trait_number, simParam = SIMPARAM)
        return(rowSums(geno))
    } else {
        return(NA)
    }
}
    
heterozygous_load <- function(population, trait_number, SIMPARAM) {
    if (population@nInd > 0) {
        geno <- pullQtlGeno(population, trait_number, simParam = SIMPARAM)
        return(rowSums(geno == 1))
    } else {
        return(NA)
    }
}

homozygous_load <- function(population, trait_number, SIMPARAM) {
    if (population@nInd > 0) {
        geno <- pullQtlGeno(population, trait_number, simParam = SIMPARAM)
        return(rowSums(geno == 2) * 2)
    } else {
        return(NA)
    }
}

population_summaries <- function(population, SIMPARAM) {
    data.frame(segregating_performance_qtl = length(segregating_qtl(population, 1, SIMPARAM)),
               segregating_condition_qtl = length(segregating_qtl(population, 2, SIMPARAM)),
               mean_load =  mean(total_load(population, 2, SIMPARAM)),
               mean_heterozyous_load = mean(heterozygous_load(population, 2, SIMPARAM)))
}


deleterious_allele_frequencies <- function(population, SIMPARAM) {
    condition_geno <- pullQtlGeno(population, 2, simParam = SIMPARAM)
    f <- colSums(condition_geno)/(2 * nrow(condition_geno))
    daf <- data.frame(qtl = names(f), frequency = f, stringsAsFactors = FALSE)
    daf$effect <- SIMPARAM@traits[[2]]@addEff
    daf$locus_load <- 2 * daf$frequency * (1 - daf$frequency) * daf$effect * 0.5 + 2 * daf$frequency^2 * daf$effect
    daf$large <- daf$effect > 0.1
    daf
}

snp_allele_frequencies <- function(population, SIMPARAM) {
    snp_geno <- pullSnpGeno(population, 1, simParam = SIMPARAM)
    f <- colSums(snp_geno)/(2 * nrow(snp_geno))
    data.frame(qtl = names(f), frequency = f, stringsAsFactors = FALSE)
}
