# RAGE against the deleterious load # 

This repository contains simulation code for the manuscript:

Johnsson et al. Removal of alleles by genome editing -- RAGE against
the deleterious load


R packages used:

* AlphaSimR 0.3.3
* ggplot2 2.2.1
* plyr 1.8.4
* assertthat 0.2.0
* methods 3.4.3
* Rcpp 0.12.14
* magrittr 1.5
